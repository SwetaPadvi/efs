<?php
App::uses('AppModel', 'Model');
/**
 * Stock Model
 *
 * @property Customer $Customer
 */
class Stock extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'customer_id';
	public $validate = array(
		'id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please provide id for the customer.'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
		 'stsymbol' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please provide stsymbol.',
				'allowEmpty' => false,
				'required' => false
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		 'stname' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please provide stname.',
				'allowEmpty' => false,
				'required' => false
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			   ),
			),
			 'noshares' => array(
		
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please provide Noshares.',
				'allowEmpty' => false,
				'required' => false
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			// 'rule' => '/^[a-zA-Z]+$/',
			  'rule' => '/^[0-9]+$/',
			'message' => 'Please provide Noshares in numeric value.'
		),
		
		 'purchaseprice' => array(
		
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please provide Purchaseprice.',
				'allowEmpty' => false,
				'required' => false
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			// 'rule' => '/^[a-zA-Z]+$/',
			  'rule' => '/^[0-9]+$/',
			'message' => 'Please provide Purchaseprice in numeric value.'
		),
		);


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
