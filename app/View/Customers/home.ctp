

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Eagle Financials</title>
</head>
<body>
<div class="main" width="100%">
	<div class="content container">
		
	   <div  id="mission" height="80%">
		<h2>Our Mission</h2>
		
        <p>Empower people with resources they own.Be more.</p>
        <p>We strive to be your financial experts from youth through the senior years. We build effective investment plans for each and every one of our customers and its very much customized, so that your money receives the most appropriate care at crucial milestones. We want to give you assured financial&nbsp;life.</p>

	</div><!-- mission page -->
	
		<div class="row">
		<article class="customers col-sm-4">
		<img class="icon" src="img/customer.png" alt="Icon"></br>
	   
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href="/eagle/customers">View Customers</a></p>
	  
		</article>
		<article class="Investments col-sm-4">
		<img class="icon" src="img/investment.png" alt="Icon"></br>
	   
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href="/eagle/investments">View Investments</a></p>
	  
		</article>
		
		
		</article>
		<article class="Stocks col-sm-4">
		<img class="icon" src="img/Stocks.png" alt="Icon"></br>
	   
		&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href="/eagle/stocks">View Stocks</a></p>
	  
		</article>
		
		</div>
	</div>

</div>


</br>
</br>
</br>
</br>
</br>

    <footer>
			<div class="container">
			<div class="row">
			<div class"col-sm-6">
    <p>Call us toll-free at <span class="phone">1234-555-1212</span></p>
		<p>All contents &copy; 2016 <a href="/eagle">Eagle Financial Services</a>. All rights reserved.</p>
			</div><!--col-sm-6-->
			</br>
	<div class"col-sm-6">
    <nav class="navbar navbar-default" role="navigation" >
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Terms of use</a></li>
            <li><a href="#">Privacy policy</a></li>
			</ul>
				</nav>
					</div><!--col-sm-6-->
				</div><!--row-->
			</div><!--container -->
		</footer>
</body>

</html>