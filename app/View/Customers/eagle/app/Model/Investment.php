<?php
App::uses('AppModel', 'Model');
/**
 * Investment Model
 *
 * @property Customer $Customer
 */
class Investment extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'customer_id';
		public $validate = array(
		'id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please provide id for the customer.'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'category' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please provide category.',
				'allowEmpty' => false,
				'required' => false
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
		  ),
		  ),
		  'acquiredvalue' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please provide Acquiredvalue.',
				'allowEmpty' => false,
				'required' => false
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
		'acquireddate' => array(
		 'rule' => 'date',
          'message' => 'Enter a valid date'
			
		),
		 'recentvalue' => array(
		
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please provide recentvalue.',
				'allowEmpty' => false,
				'required' => false
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			// 'rule' => '/^[a-zA-Z]+$/',
			  'rule' => 'numeric',
			'message' => 'Please provide recentvalue in numeric value.'
		),
		
		'recentdate' => array(
		 'rule' => 'date',
          'message' => 'Enter a valid date'
			
		),
		
		
		
		);


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
