<?php
App::uses('AppModel', 'Model');
/**
 * Customer Model
 *
 * @property Investment $Investment
 * @property Stock $Stock
 */
class Customer extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
 
 
	public $validate = array(
		'id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please provide id for the customer.',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
		
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Please provide name for the customer.',
				'allowEmpty' => false,
				'required' => false
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			// 'rule' => '/^[a-zA-Z]+$/',
			'rule' => '/^[a-zA-Z ]*$/',
			'message' => 'Please provide character for the customer.'
		),
		'zip' => array(
				'rule' => '/^[0-9]+$/',
				'message' => 'Please provide zip in numeric.'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
	  'primaryemail' => array(
				     'rule' => array('email', true),
                     'message' => 'Please supply a valid email address.'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
		 'homephone' => array(
		 'rule' => array('phone', null, 'all'),
		 'message' => 'Please supply a valid phone number.' 
			
		),
		'cellphone' => array(
		 'rule' => array('phone', null, 'all'),
		 'message' => 'Please supply a valid phone number.' 
			
		),
		
	);
		

		
		
		
		
		
		
		
		
	

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Investment' => array(
			'className' => 'Investment',
			'foreignKey' => 'customer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Stock' => array(
			'className' => 'Stock',
			'foreignKey' => 'customer_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
