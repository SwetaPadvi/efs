
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	<meta name="viewport" content="width=device-width ,initial-scale=1">
</head>
<body>
<div class="stocks view">
<h2><?php echo __('Stock'); ?></h2>
		<table cellpadding = "0" cellspacing = "0" border="0">
		<tr>
		<td><b><?php echo __('Id'); ?></b></td>
		<td><?php echo h($stock['Stock']['id']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Customer'); ?></b></td>
		<td><?php echo $this->Html->link($stock['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $stock['Customer']['id'])); ?>
		</td>
		</tr>
		<tr>
		<td><b><?php echo __('Stsymbol'); ?></b></td>
		<td><?php echo h($stock['Stock']['stsymbol']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Stname'); ?></b></td>
		<td><?php echo h($stock['Stock']['stname']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Noshares'); ?></b></td>
		<td><?php echo h($stock['Stock']['noshares']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Purchaseprice'); ?><b></td>
		<td><?php echo h($stock['Stock']['purchaseprice']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Datepurchased'); ?></b></td>
		<td><?php echo h($stock['Stock']['datepurchased']); ?>&nbsp;</td>
		</tr>
		<tr>
		<td><b><?php echo __('Created'); ?></b></td>
		<td><?php echo h($stock['Stock']['created']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Modified'); ?></b></td>
		<td><?php echo h($stock['Stock']['modified']); ?></td>
		</tr>
	</table>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Stock'), array('action' => 'edit', $stock['Stock']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Stock'), array('action' => 'delete', $stock['Stock']['id']), array(), __('Are you sure you want to delete # %s?', $stock['Stock']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Stocks'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stock'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>
</body>
</html>
