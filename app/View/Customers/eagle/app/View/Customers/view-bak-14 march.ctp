<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width ,initial-scale=1">
	
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	
	<div class="customers view">

	<div class="container">
		<h2><?php echo __('Customer'); ?></h2>
		  <div class="table-responsive">  
	
	<table cellpadding = "0" cellspacing = "0" border="0">
		<tr>
		<td><b><?php echo __('Id'); ?></b></td>
		<td><?php echo h($customer['Customer']['id']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Name'); ?></b></td>
		<td><?php echo h($customer['Customer']['name']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Streetaddress'); ?></b></td>
		<td><?php echo h($customer['Customer']['streetaddress']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('City'); ?></b></td>
		<td><?php echo h($customer['Customer']['city']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('State'); ?></b></td>
		<td><?php echo h($customer['Customer']['state']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Zip'); ?></b></td>
		<td><?php echo h($customer['Customer']['zip']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Primaryemail'); ?></b></td>
		<td><?php echo h($customer['Customer']['primaryemail']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Homephone'); ?></b></td>
		<td><?php echo h($customer['Customer']['homephone']); ?>&nbsp;</td>
		</tr>
		<tr>
		<td><b><?php echo __('Cellphone'); ?></b></td>
		<td><?php echo h($customer['Customer']['cellphone']); ?>&nbsp;</td>
		</tr>
		<tr>
		<td><b><?php echo __('Created'); ?></b></td>
		<td><?php echo h($customer['Customer']['created']); ?>&nbsp;</td>
		</tr>
		<tr>
		<td><b><?php echo __('Modified'); ?></b></td>
		<td><?php echo h($customer['Customer']['modified']); ?>&nbsp;</td>
	    </tr>
		</table>
    </div>
	 </div>
</div>    
  

<div class="actions">
<div class="container">

	<h3><?php echo __('Actions'); ?></h3>
	<ul class="list-group">
		<li><?php echo $this->Html->link(__('Edit Customer'), array('action' => 'edit', $customer['Customer']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Customer'), array('action' => 'delete', $customer['Customer']['id']), array(), __('Are you sure you want to delete # %s?', $customer['Customer']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Investments'), array('controller' => 'investments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Investment'), array('controller' => 'investments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Stocks'), array('controller' => 'stocks', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Stock'), array('controller' => 'stocks', 'action' => 'add')); ?> </li>
	</ul>
	</div>
	</div>

 
 
	
<div class="related">
	
	<?php if (!empty($customer['Investment'])): ?>
	<h3><?php echo __('Related Investments'); ?></h3>
	
	<table cellpadding = "0" cellspacing = "0">
		
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('Category'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Acquiredvalue'); ?></th>
		<th><?php echo __('Acquireddate'); ?></th>
		<th><?php echo __('Recentvalue'); ?></th>
		<th><?php echo __('Recentdate'); ?></th>
	</tr>
	
	<?php $Acquiredvalsum=0;?>
	<?php $Recentvalsum=0;?>
	<?php foreach ($customer['Investment'] as $investment): ?>
	
		<tr>
			<td><?php echo $investment['id']; ?></td>
			<td><?php echo $investment['customer_id']; ?></td>
			<td><?php echo $investment['category']; ?></td>
			<td><?php echo $investment['description']; ?></td>
			<td><?php echo $investment['acquiredvalue']; ?></td>
						<?php $Acquiredvalsum+=$investment['acquiredvalue']; ?>
			<td><?php echo $investment['acquireddate']; ?></td>
			<td><?php echo $investment['recentvalue']; ?></td>
			<?php   $Recentvalsum+=$investment['recentvalue']; ?>
			<td><?php echo $investment['recentdate']; ?></td>
		</tr>
		
	<?php endforeach; ?>
		
	<tr>
	<td colspan=4></td>
	<td>
	<strong><?php echo "$".$Acquiredvalsum; ?></strong>
	</td>
	<td></td>
	<td><strong><?php echo "$".$Recentvalsum; ?></strong></td>
	<td></td>
	<td>
	
	</td>
	</tr>
	
	
	
	
	</table>
<?php endif; ?>


	<div class="related">
	
	 
	   
	   
		

		
		
				<?php if (!empty($customer['Stock'])): ?>
				
		<h3><?php echo __('Related Stocks'); ?></h3>
		

		<table cellpadding = "0" cellspacing = "0">
			
		<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Customer Id'); ?></th>
		<th><?php echo __('Stsymbol'); ?></th>
		<th><?php echo __('Stname'); ?></th>
		<th><?php echo __('Noshares'); ?></th>
		<th><?php echo __('Purchaseprice'); ?></th>
		<th><?php echo __('Datepurchased'); ?></th>
		<th><?php echo __('Original Value'); ?></th>
		<th><?php echo __('Current Price'); ?></th>
		<th><?php echo __('Current Value'); ?></th>
		</tr>
		
		
		
		<?php $GLOBAL['x']=0; ?>
		
			<?php $sum=0;?>
			<?php $currentvaluesum=0;?>
	<?php foreach ($customer['Stock'] as $x =>$stock): ?>   
	   
	  

	   <tr>
	        	<td><?php echo $stock['id']; ?></td>
			
			<td><?php echo $stock['customer_id']; ?></td>
			<td><?php echo $stock['stsymbol']; ?></td>
			<td><?php echo $stock['stname']; ?></td>
			<td><?php echo $stock['noshares']; ?></td>
			<td><?php echo $stock['purchaseprice']; ?></td>
			<td><?php echo $stock['datepurchased']; ?></td>
			<td><?php echo $stock['originalvalue']=$stock['noshares']*$stock['purchaseprice'];?></td>
			<?php $sum+=$stock['originalvalue']; ?>
			<td><?php
     
        $quote=$stock['stsymbol'];
        require_once('nusoap.php');

        $c = new nusoap_client('http://loki.ist.unomaha.edu/~groyce/ws/stockquoteservice.php');

        $stockprice = $c->call('getStockQuote',
            array('symbol' => $quote));

			 echo $stock['currentprice']=$stockprice; ?></td>
			 <td><?php  echo $stock['currentvalue']=$stock['noshares']*$stock['currentprice']; ?></td>
		<?php $currentvaluesum+=$stock['currentvalue']; ?>
	     </tr>
		 </div>
	

	
             
	<?php endforeach; ?>
		
	<tr>
	<td colspan=7></td>
	<td>
	<strong><?php echo "$".$sum; ?></strong>
	</td>
	<td></td>
	<td><strong><?php echo "$".$currentvaluesum; ?></strong></td>
	</tr>
	
		</table>

	</div>
	</div>

	<?php endif; ?>

	
		<div class="related">
		
		<h3>Total Portfolio Value</h3>
	
	 <table cellpadding = "0" cellspacing = "0">
	 <tr>
	 
		<th ><?php echo "Category" ?></th>
		<th ><?php echo "Original Value" ?></th>
		<th ><?php echo "Current Value" ?></th>
	 </tr>
	 
		<tr>
	<td >stocks</td>
	<td><?php echo $sum; ?></td>
	<td><?php echo $currentvaluesum; ?></td>
	</tr>
	<tr>
	<td >Investments</td>
	<td><?php echo $Acquiredvalsum; ?></td>
	<td><?php echo $Recentvalsum; ?></td>
	</tr>
	
	<td ><strong>Total Portfolio<strong></td>
	<td><strong><?php echo $Acquiredvalsum+$sum; ?><strong></td>
	<td><strong><?php echo $Recentvalsum+$currentvaluesum; ?><strong></td>
	</tr>
	 </table>
	
	
	 </div>

	
	
	
	
	
	
	
	
	
</body>
</html>
