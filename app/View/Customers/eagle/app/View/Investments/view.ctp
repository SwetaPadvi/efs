
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	<meta name="viewport" content="width=device-width ,initial-scale=1">
</head>
<body>

<div class="investments view">
<h2><?php echo __('Investment'); ?></h2>
		<table cellpadding = "0" cellspacing = "0" border="0">
		<tr>
		<td><b><?php echo __('Id'); ?></b></td>
		<td><?php echo h($investment['Investment']['id']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Customer'); ?></b></td>
		<td><?php echo $this->Html->link($investment['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $investment['Customer']['id'])); ?>
		</td>
		</tr>
		<tr>
		<td><b><?php echo __('Category'); ?></b></td>
		<td><?php echo h($investment['Investment']['category']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Description'); ?></b></td>
		<td><?php echo h($investment['Investment']['description']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Acquiredvalue'); ?></b></td>
		<td><?php echo h($investment['Investment']['acquiredvalue']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Acquireddate'); ?></b></td>
		<td><?php echo h($investment['Investment']['acquireddate']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Recentvalue'); ?></b></td>
		<td><?php echo h($investment['Investment']['recentvalue']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Recentdate'); ?></b></td>
		<td><?php echo h($investment['Investment']['recentdate']); ?>&nbsp;</td>
		</tr>
		<tr>
		<td><b><?php echo __('Created'); ?></b></td>
		<td><?php echo h($investment['Investment']['created']); ?></td>
		</tr>
		<tr>
		<td><b><?php echo __('Modified'); ?></b></td>
		<td><?php echo h($investment['Investment']['modified']); ?>&nbsp;</td>
	    </tr>
		</table>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Investment'), array('action' => 'edit', $investment['Investment']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Investment'), array('action' => 'delete', $investment['Investment']['id']), array(), __('Are you sure you want to delete # %s?', $investment['Investment']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Investments'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Investment'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>

</body>
</html>