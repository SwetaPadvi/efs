<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	
	public $components = array('Session');
	
	/*
	 public function initialize(Controller $controller) {
        $this->Auth->authenticate = array('Auth' => array(
       'loginRedirect' => array(
       'controller' => 'users',
       'action' => 'index'
         ),)
		 
		 );
    }
	public function beforeFilter(){
		
		
		$this->Auth->allow('*');
	}
	*/
	/*
	public function beforeFilter(){
		
		$this->configAuth();
		$this->Auth->allow();
	}
	
	
	private function configAuth(){
		
		$this->Auth->authenticate = array(
		'all'=>array('userModel' => 'User','fields'=>array('password'=>'password','username'=>'username'),),'Form',);
		
		$this->Auth->LoginAction='/login';
		$this->Auth->LogoutAction='/logout';
	    $this->Auth->LoginRedirect='/customers';
        $this->Auth->LoginError='Please enter valid username or password!';
		
		}
	
	*/
		/*
		
		function beforeFilter(){
    // loginAction defines the action which should be used to login. By default users/login
    $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
 
    // loginRedirect defines the action called after user is logged in first time.
    $this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'display', 'home');
 
    // Allows the display action to be accessed without user login.
    $this->Auth->allow('display');
    $this->Auth->authorize = 'controller';
}
*/
}
